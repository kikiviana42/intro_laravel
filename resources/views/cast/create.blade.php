@extends('layout.master')

@section('title')
    Tambah Cast
@endsection 

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama Cast</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Cast">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Cast">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">bio</label>
        <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio Cust">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>


@endsection