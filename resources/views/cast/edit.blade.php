@extends('layout.master')

@section('title')
    Detail Cast id ke {{$cast->id}}
@endsection 

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama">Nama Cast</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama Cast">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur Cast">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">bio</label>
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="bio" placeholder="Masukkan Bio Cust">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection